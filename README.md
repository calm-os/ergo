# Ergo

A toolkit and Human Interface Guidelines for creating beautiful, accessible,
converged, and portable applications, for all devices.

## Introduction 

Ergo is a toolkit for building beautiful, accessible, converged, and portable
applications, based on Qt Quick Controls 2. The project is a hard fork of
the Ubuntu UI Toolkit developed for the Unity 8 and Ubuntu for devices
platform, in an effort to evolve the style defined there, and improve
performance, maintainability, and ease of use.

## Donate

Please consider donating to help cover the costs of developing and maintaining
the project. You can send donations with the following methods. To donate using
other cryptocurrencies, use the address from the _Monero_ link below, as the
_receive_ address on an exchange such as [ChangeNOW](https://changenow.io/) or
similar. Thank you.

* [Monero](monero:87apvQxCW7LgCPjz7muRBedPf19TexjFAT3gjdyVKk6bVdD5cr7NGohbYCC6JKtKeM2a93tiWmTzj7hho4fRaZav5rJd7Y1)
* [LiberaPay](https://liberapay.com/dobey)
* [CashApp](https://cash.app/$dohbee)
* [Patreon](https://patreon.com/dobey)

## License

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation; version 3.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
