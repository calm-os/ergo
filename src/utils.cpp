/*
 * Copyright 2019 Rodney Dawes
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "utils.h"

#include <cstdlib>

#include <QCoreApplication>
#include <QRegularExpression>
#include <QString>
#include <QtGlobal>


namespace ergo
{

std::string DirUtils::appPackageDir()
{
    // First try for click packages on Ubuntu Touch
    auto appDir = qgetenv("APP_DIR").toStdString();
    if (!appDir.empty()) {
        return appDir;
    }

    // Next check if we're a snap package on Linux
    appDir = qgetenv("SNAP").toStdString();
    if (!appDir.empty()) {
        return appDir;
    }

    // Next use Qt's applicationDirPath, and strip `bin` from the end
    auto path = QCoreApplication::applicationDirPath();
    if (path.endsWith("/bin")) {
        return path.remove(QRegularExpression("/bin$")).toStdString();
    } else {
        return path.toStdString();
    }

    // If everything else fails, fall back to `/usr` as prefix
    return std::string{"/usr"};
}

} // namespace ergo
