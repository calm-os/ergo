/*
 * Copyright 2015 Canonical Ltd.
 * Copyright 2018-2023 Rodney Dawes
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import Ergo 0.0
import QtQuick 2.12
import QtQuick.Controls 2.12
import QtGraphicalEffects 1.0

/*! The Icon component displays a symbolic themed icon which may have its color
 *  swapped with the specified color.
 *
 *  Using icons whenever possible enhances consistency accross applications.
 *  Each icon has a name and can have different visual representations
 *  depending on the size requested.
 *
 *  Example:
 *  \code
 *  Icon {
 *      width: 48
 *      height: 48
 *      name: "go-previous"
 *  }
 *  \endcode
 *
 *  Example of colorization:
 *  \code
 *  Icon {
 *      width: 48
 *      height: 48
 *      name: "go-previous"
 *      color: "red"
 *  }
 *  \endcode
 *
 *  Icon themes are created following the
 *  [Freedesktop Icon Theme Specification](http://standards.freedesktop.org/icon-theme-spec/icon-theme-spec-latest.html).
 */
Item {
    id: iconRoot

    /** The named icon to use from the theme. **/
    property string name

    /** type:url A URL source for the Icon. Overrides \ref name if set. **/
    property alias source: iconImage.source

    /** type:color The color to colorize the Icon with. **/
    property color color: "transparent"

    /** \deprecated type:color The color to be replaced in the Icon when colorizing. **/
    property color baseColor: "transparent"

    Label {
        id: label
        width: 0
        height: 0
        visible: false
    }

    Image {
        id: iconImage
        objectName: "iconMask"
        anchors.fill: parent
        fillMode: Image.PreserveAspectFit

        cache: true
        visible: false

        sourceSize.width: width
        sourceSize.height: height

        readonly property string rtl: Qt.application.layoutDirection == Qt.RightToLeft ? "-rtl" : ""
        source: iconRoot.name ? "image://theme/%1%2".arg(iconRoot.name).arg(rtl) : ""
    }

    ColorOverlay {
        id: colorMask
        objectName: "colorMask"

        anchors.fill: iconImage
        cached: iconImage.cache
        color: Qt.colorEqual(iconRoot.color, "transparent") ? label.color : iconRoot.color
        source: iconImage
        visible: true
    }
}
