/*
 * Copyright 2019 Rodney Dawes
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "../utils.h"

#include <QDir>
#include <QString>
#include <QtGlobal>
#include <QtTest/QtTest>

class TestUtils: public QObject
{
    Q_OBJECT

private slots:
    void appPackageDir_data();

    void appPackageDir();
};

void TestUtils::appPackageDir_data()
{
    QTest::addColumn<QString>("env_var");
    QTest::addColumn<QString>("expected");

    QTest::newRow("click package") << QStringLiteral("APP_DIR") << QStringLiteral("/opt/click.ubuntu.com/test.test/current");
    QTest::newRow("snap package") << QStringLiteral("SNAP") << QStringLiteral("/snap/test.test");
    QTest::newRow("system app") << QStringLiteral("") << QDir::currentPath();
}

void TestUtils::appPackageDir()
{
    QFETCH(QString, env_var);
    QFETCH(QString, expected);

    qputenv(env_var.toUtf8().constData(), expected.toUtf8());
    auto result = ergo::DirUtils::appPackageDir();
    QCOMPARE(result, expected.toStdString());
    qunsetenv(env_var.toUtf8().constData());
}

QTEST_MAIN(TestUtils)
#include "test-utils.moc"
