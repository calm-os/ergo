/*
 * Copyright © 2020-2023 Rodney Dawes
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import QtQuick.Templates 2.12 as T


T.TextField {
    id: control
    implicitHeight: 36

    background: Rectangle {
        anchors.fill: parent
        color: "#232323"
        border.color: (control.displayText.length == 0 && !control.activeFocus) || control.acceptableInput ? "#5d5d5d" : "#ed3434"
        border.width: 2
        radius: 8
    }

    padding: 4
    leftPadding: 8
    rightPadding: leftPadding
    font.pixelSize: 18
    verticalAlignment: Text.AlignVCenter
    color: "#efefef"
    placeholderTextColor: "#5d5d5d"
    selectedTextColor: "#ececec"
    selectionColor: "#3434bb"
    selectByMouse: true

    Text {
        id: placeholder
        anchors {
            fill: parent
            leftMargin: control.leftPadding
            rightMargin: control.rightPadding
            topMargin: control.topPadding
            bottomMargin: control.bottomPadding
        }

        text: control.placeholderText
        font: control.font
        color: control.placeholderTextColor
        horizontalAlignment: control.horizontalAlignment
        verticalAlignment: control.verticalAlignment
        visible: !control.length && !control.preeditText
        renderType: control.renderType
    }
}
