/*
 * Copyright © 2020-2023 Rodney Dawes
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import QtQuick.Templates 2.12 as T


T.SpinBox {
    id: control
    implicitHeight: 36

    background: Rectangle {
        anchors.fill: parent
        color: "#232323"
        border.color: input.acceptableInput ? "#5d5d5d" : "#ed3434"
        border.width: 2
        radius: 8
    }

    down.indicator: Item {
        id: dnIndicator
        x: 0
        height: parent.height
        width: 24
        Label {
            anchors.centerIn: parent
            font.pixelSize: 20
            text: "-"
        }
    }
    up.indicator: Item {
        id: upIndicator
        x: parent.width - width
        height: parent.height
        width: 24
        Label {
            anchors.centerIn: parent
            font.pixelSize: 20
            text: "+"
        }
    }

    validator: IntValidator {
        locale: control.locale.name
        bottom: control.from
        top: control.to
    }

    contentItem: TextInput {
        id: input

        z: 2
        clip: true

        anchors.left: dnIndicator.right
        anchors.right: upIndicator.left
        width: control.width - dnIndicator.width * 2
        text: control.displayText
        leftPadding: 4
        rightPadding: leftPadding
        color: "#efefef"
        selectedTextColor: "#ececec"
        selectionColor: "#3434bb"
        font.pixelSize: 18
        verticalAlignment: Qt.AlignVCenter
        horizontalAlignment: Qt.AlignRight

        readOnly: !control.editable
        validator: control.validator
        inputMethodHints: control.inputMethodHints

        // TextEdited/EditingFinished are needed to enable immediate feedback
        // Unfortunately, may result in errors due to an issue in Qt upstream:
        // https://bugreports.qt.io/browse/QTBUG-64151
        onTextEdited: {
            control.value = control.valueFromText(text, control.locale);
        }
        onEditingFinished: {
            control.value = control.valueFromText(text, control.locale);
            input.text = control.textFromValue(control.value, control.locale);
        }
        onActiveFocusChanged: {
            if (activeFocus && control.editable) {
                selectAll();
            }
        }
    }

    onValueModified: {
        input.text = control.textFromValue(control.value, control.locale);
    }
    Component.onCompleted: {
        valueModified();
    }
}
