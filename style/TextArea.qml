/*
 * Copyright © 2020-2023 Rodney Dawes
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import QtQuick.Templates 2.12 as T


T.TextArea {
    id: control

    background: Rectangle {
        color: "#232323"
        border.color: "#5d5d5d"
        border.width: 2
    }

    padding: 4
    color: "#efefef"
    selectedTextColor: "#ececec"
    selectionColor: "#3434bb"
    selectByMouse: true
}
