/*
 * Copyright © 2020-2023 Rodney Dawes
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import Ergo 0.0
import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Templates 2.12 as T


T.ComboBox {
    id: control
    implicitHeight: 36

    delegate: ItemDelegate {
        height: control.height - 8
        width: control.width - 12
        x: 4
        contentItem: Rectangle {
            color: "transparent"
            anchors.fill: parent
            Text {
                anchors.fill: parent
                leftPadding: 4
                color: "#efefef"
                text: modelData
                font.bold: control.highlightedIndex === index
                verticalAlignment: Text.AlignVCenter
                font.pixelSize: 14
            }
        }
        highlighted: false
    }

    indicator: Icon {
        x: control.width - width / 2 - 12
        y: (control.availableHeight - height / 2) / 2
        height: control.height / 2
        width: height
        name: "go-down-symbolic"
        color: "#efefef"
    }

    background: Rectangle {
        anchors.fill: parent
        color: "#232323"
        border.color: "#5d5d5d"
        border.width: 2
        radius: 10
    }

    contentItem: Text {
        anchors.fill: parent
        leftPadding: 16
        padding: 4
        color: "#efefef"
        text: control.displayText
        verticalAlignment: Text.AlignVCenter
        font.pixelSize: 14
    }

    popup: Popup {
        y: control.height - 1
        width: control.width
        implicitHeight: contentItem.implicitHeight
        padding: 2

        contentItem: ListView {
            clip: true
            implicitHeight: contentHeight
            model: control.delegateModel
            currentIndex: control.highlightedIndex
            delegate: control.delegate
        }

        background: Rectangle {
            width: control.width
            implicitHeight: contentItem.implicitHeight
            color: "#343434"
            border.color: "#232323"
            border.width: 2
            radius: 10
        }
    }
}
